Une course cycliste se dispute. 
![course](https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/V%C3%A9locourse.jpg/220px-V%C3%A9locourse.jpg)

Les cyclistes se doublent les uns les autres durant la course.

Il peut arriver qu'un cycliste crève, et se fasse doubler par tous les autres coureurs et se retrouve à la dernière place.

Les coureurs sont identifiés par leur prénom et leurs positions sont stockées de façon ordonnée dans un tableau.

Exemple :
```python
course = ["Nadia", "Eric", "Thomas", "Elizabeth", "Laure"]
```
Dans cet exemple, Nadia est la première et Laure la dernière.

* **Créer** la fonction `premier` qui prend en paramètre le tableau qui stocke la course et qui renvoie le coureur en première position de la course.
* **Créer** la fonction `dernier` qui prend en paramètre le tableau qui stocke la course et qui renvoie le coureur en dernière position de la course.
* **Créer** la fonction `double` qui prend en paramètre le tableau qui stocke la course et la position d'un coureur et modifie le tableau avec le coureur qui est passé devant le coureur précédent.
* **Créer** la fonction `crevaison` qui prend en paramètre le tableau qui stocke la course et la position d'un coureur qui a crevé et modifie le tableau. On partira du principe que tous les coureurs derrière le malchanceux coureur le doublent jusqu'à ce que le coureur soit le dernier.

On vérifiera les préconditions sur les paramètres.
