﻿#Question 2

# exemple quelconque
tableau = [12, 10, 13, 14, 18, 17]
assert temperature_max(tableau) == 18

# utilisation avec les données de l'ISS
assert temperature_max(temperature) == 35.0


# utilisation avec les données de l'ISS
assert temperature_max(table) == ('35.0', '2022-02-01 07:22:30.801076')

# utilisation avec les données de l'ISS
assert temperature_max2(table) == (35.0, '2022-02-01 07:22:30.801076')

#Question 3

# exemple quelconque
tableau = [12, 10, 13, 14, 18, 17]
assert isclose(temperature_moy(tableau), 14.0)

# utilisation avec les données de l'ISS
assert isclose(temperature_moy(temperature), 34.08995308552)

assert pression_hum_moy(table) == (1013.92, 30.27)


#Question 4

# exemple quelconque
tableau = [12, 10, 13, 14, 18, 17]
assert occurrence(tableau,14) == True
assert occurrence(tableau,15) == False

# utilisation avec les données de l'ISS
reset = [int(table[k][21]) for k in range(1, len(table))]
assert occurrence(reset,1000) == True
assert occurrence(reset,2) == False


assert occurrences(table, 1000) == (1, 13248)
assert occurrences(table, 5) == (None, None)