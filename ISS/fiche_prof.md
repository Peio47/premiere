# Ficher prof pour l'activité : Traitement des données de l'ISS

## Objectifs

Découvrir les algorithmes de base à connaître sur les parcours de tableau : moyenne, maximum, occurrence

## Prérequis 

Connaissane fichier CSV
Parcours d'un tableau

## Durée de l'activité

2 heures

## Exercices cibles : 

On indique l'algorithme à utiliser et l'élève doit le traduire en langage Python.
Pour chaque algorithme on propose un exemple simple, vérifiable à la main, et un test issu des données de l'ISS.

## Description du déroulement de l'activité : 

 - Présentation des données sur lesquelles les élèves vont travailler. (3 min)
 - Présentation de l'algorithme du maximum. (5 min) Réalisation de l'exercice par les élèves.(20 min) Correction par un élève. (5 min)
 - Présentation de l'algorithme de la moyenne. (5 min) Réalisation de l'exercice par les élèves.(20 min) Correction par un élève. (5 min)
 - Présentation de l'algorithme de la recherche d'une occurrence. (5 min) Réalisation de l'exercice par les élèves.(20 min) Correction par un élève. (5 min)
 - Réalisation des exercices d'application(20 min). Présentation des solutions par les élèves (10 min)

## Anticipation des difficultés des élèves :

Le travail se veut totalement individuel, charge à l'enseignant d'apporter de l'aide sur

    * la manipulation de l'interpréteur
    * aide au déboggage
    * syntaxe python de la boucle bornée sur demande
    * Aide lors de l'écriture des fonctions



## Gestion de l'hétérogénéïté :

Les exercices appelés bis sont destinés à appliquer l'algorithme dans un programme plus complexe. Il permet aux élèves plus rapides de pouvoir aller plus loin.
