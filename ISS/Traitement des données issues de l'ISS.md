# Traitement des données issues de l'ISS

L'objectif de cette activité est de savoir réaliser un traitement sur des valeurs contenues dans un tableau.

Pour ce faire, nous allons traiter des données relevées pendant deux jours à bord du module européen Columbus de la station spatiale internationale.

Attention : les données sont présentes dans le fichier CSV sous forme de chaîne de caractères. Avant de les traiter, il faudra impérativement les convertir en entier ou flottant.

````python
from csv import reader
from math import isclose

with open("AstroPI_31012022.csv", mode='r') as fichier_ouvert:
    table = list(reader(fichier_ouvert,delimiter=","))
````

Il est possible d'extraire une partie des informations de cette table.

````python
temperature = [float(table[k][3]) for k in range(1, len(table))]
````

Q1) **Justifier** que l'instruction précédente permet de récupérer l'ensemble des données de température à l'intérieur du module Columbus.

## Recherche d'un maximum

On souhaite déterminer la température maximale à bord.

L'algorithme de recherche d'un maximum est le suivant :

```
maximum <- première valeur 
Pour chaque valeur du tableau
    Si valeur > maximum alors
        maximum <- valeur
    fin si   
fin pour
```

Q2) **Créer** la fonction `temperature_max` qui prend en paramètre un tableau de réels et renvoie la valeur maximale.

Q2 - bis) **Compléter** la fonction `temperature_max2` qui prend en paramètre la table complète de données et renvoie la valeur maximale ainsi que la date à laquelle elle a été relevée.

## Calcul de la moyenne

On souhaite à présent calculer la température moyenne à bord.

L'algorithme de calcul de la moyenne est le suivant :

```
somme <- 0
Pour chaque valeur du tableau
    Si valeur > maximum alors
        somme <- somme + valeur
    fin si
moyenne <- somme / nbr_element_tableau
```   

Q3) **Créer** la fonction `temperature_moy` qui prend en paramètre un tableau de réels et renvoie la valeur moyenne.

Q3 - bis) **Créer** la fonction `pression_hum_moy` qui prend en paramètre la table de données et renvoie les valeurs moyennes de pression et d'humidité.

On arrondira les valeurs à deux chiffres après la virgule avec la fonction `round`.

## Recherche d'une occurrence

La colonne *rset* indique une réinitialisation du processeur du raspberry pi.
La valeur est à 1000 lors de la fin de la réinitialisation

L'algorithme de de recherche d'une occurrence est le suivant :

```
valeur_trouve <- Faux
Pour chaque valeur du tableau
    Si valeur = valeur_cherchée alors
        valeur_trouve <- Vrai
    fin si
fin pour
```  

Q4) **Créer** la fonction `occurrence` qui prend en paramètre un tableau non vide d'entiers et une valeur cherchée et renvoie un booléen.

Q4 - bis) **Créer** la fonction `occurrences` qui prend en paramètre un tableau d'entier et une valeur cherchée et renvoie l'indice de la première et de la dernière occurrence.
