# Parcours d'un fichier CSV

L'objectif de cette activité est de parcourir des données stockées dans un fichier CSV.

Pendant deux jours, des capteurs ont permis de relever un certains nombres de grandeurs à bord du module européen Columbus de la station spatiale internationale. Ce jeu de données a été enregistré dans un fichier CSV (Comma Separated Values)

## CSV et tableur

Nous allons dans un premier temps visualiser le contenu du fichier au moyen d'un tableur.

Q1) Après avoir téléchargé le fichier, **ouvrir** ce dernier à l'aide du tableur Calc.

Q2) **Identifier** les descripteurs et **comptabiliser** le nombre d'enregistrements.

Q3) En triant la colonne par ordre croissant puis décroissant, **déterminer** la température maximale et la température minimale.


## CSV et Python

A présent, nous allons étudier comment récupérer ces valeur dans un programme.

Ces données peuvent se présenter soit sous forme d'un tableau de tableaux, soit sous la forme d'un tableau de dictionnaires.

1. **Données sous forme de tableaux**

On doit d'abord import la fonction reader du module csv. Ensuite on ouvre le fichier CSV et on stocke les données dans un tableau :

````python
from csv import reader

with open("AstroPI_31012022.csv", mode='r') as fichier_ouvert:
    table_t = list(reader(fichier_ouvert,delimiter=","))
````

La variable `table_t` est alors un tableau de tableaux.

Q4) Préciser l'instruction a rentré pour :
    * visualiser la ligne des descripteur,
    * visualiser le premier enregistrement,
    * visualiser la valeur de la température lors du premier enregistrement.



2. **Données sous forme de dictionnaire**

On doit d'abord import la fonction Dictreader du module csv. Ensuite on ouvre le fichier CSV et on stocke les données dans un tableau :

from csv import DictReader
with open("AstroPI_31012022.csv", 'r') as f:
	table_d = list(DictReader(f))

La variable `table_d` est alors un tableau de dictionnaires.

*Remarque :* Dans ce cas il n'y a pas d'entête, mais on retrouve les descripteurs dans chaque enregistrement.


Q5) Préciser l'instruction a rentré pour :
    * visualiser le premier enregistrement,
    * visualiser la valeur de la température lors du premier enregistrement.



3. **Extraction de données**


On souhaite pouvoir avoir des données partielles, un sous-ensemble des données initiales.

On va alors créer un nouveau tableau par compréhension.

Exemple avec un tableau de tableaux : 
````python
temperature = [float(table[k][3]) for k in range(1, len(table))]
````

Q6) *Justifier** que l'instruction précédente permet de récupérer l'ensemble des données de température à l'intérieur du module Columbus.

Exemple avec un tableau de dictionnaires : 
````python
temperature_268 = [ligne for ligne in data if float(ligne['temp_p'])>26.8]
````
Q7) Qu'obtient-on avec l'instruction précédente ?

Q8) **Donner** l'instruction nécessaire pour obtenir l'ensemble des enregistrements dont le magnétisme est supérieur à 36.5 mT.


4. Traitement des données

Maintenant on souhaite réaliser un traitement sur ces données et notamment déterminer la température maximale à bord.

L'algorithme de recherche d'un maximum est le suivant :

```
maximum <- première valeur 
Pour chaque valeur du tableau
    Si valeur > maximum alors
        maximum <- valeur
    fin si   
fin pour
```

Q9) **Ecrire** le programme qui permet de relever la température maximale à partir d'un tableau de tableaux.

Q10) **Ecrire** le programme qui permet de relever la température maximale à partir d'un tableau de dictionnaires.


