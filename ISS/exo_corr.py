
#Question 2
def temperature_max(valeurs_temp):
    maximum = valeurs_temp[0]
    for valeur in valeurs_temp :
        if valeur > maximum :
            maximum = valeur
    return maximum

def temperature_max2(donnees_iss):
    maximum = donnees_iss[1][3]
    indice_max = 0
    nb_valeurs = len(donnees_iss)
    for i in range(1, nb_valeurs) :
        if donnees_iss[i][3] > maximum :
            maximum = donnees_iss[i][3]
            indice_max = i

    return maximum, donnees_iss[indice_max][0]

#Question 3
def temperature_moy(valeurs_temp):
    somme = 0
    for valeur in valeurs_temp :
        somme = somme + valeur
    moyenne = somme/(len(valeurs_temp))
    return moyenne

def pression_hum_moy(donnees):
    somme_pression = 0
    somme_humidite = 0
    nbr_donnees = len(donnees)
    for i in range (1, nbr_donnees):
        somme_pression = somme_pression + float(donnees[i][5])
        somme_humidite = somme_humidite + float(donnees[i][4])
    moyenne_pression = round(somme_pression/nbr_donnees, 2)
    moyenne_humidite = round(somme_humidite/nbr_donnees, 2)
    return moyenne_pression, moyenne_humidite

#Question 4
def occurrence(valeurs, val_cherchee):
    for valeur in valeurs :
        if valeur == val_cherchee:
            return True
    return False

def occurrences(donnees, val_cherchee):
    ind_min = None
    ind_max = None
    nbr_donnees = len(donnees)
    for i in range(1, nbr_donnees):
        if int(donnees[i][21]) == val_cherchee:
            if ind_min is None :
                ind_min = i
            ind_max = i
    return ind_min, ind_max