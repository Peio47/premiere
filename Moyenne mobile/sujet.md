# Moyenne glissante

Un **accéléromètre** permet de déterminer les mouvement d'un objet.

Cependant il est souvent difficile d'interpréter les informations brutes issues de ce capteur car elles sont fortement bruités, c'est-à-dire sensible aux parasites extérieurs.

![signal parasité](https://gitlab.com/Peio47/premiere/-/raw/main/Moyenne%20mobile/brut.png)

Il convient alors d'appliquer un filtre à ce signal afin de le lisser et supprimer les variations rapides, comme représenté ci-dessous :
![signal lissé](https://gitlab.com/Peio47/premiere/-/raw/main/Moyenne%20mobile/signalfiltre.png)

Un filtre simple à mettre en place est la moyenne glissante. Une valeur filtrée est obtenue en faisant la moyenne des 5 dernières mesures.

On peut ainsi considérer une fenêtre de 4 mesures 

![signal lissé](https://gitlab.com/Peio47/premiere/-/raw/main/Moyenne%20mobile/filtre1.png)

qui va "glisser" à chaque nouvelle mesure.

![signal lissé](https://gitlab.com/Peio47/premiere/-/raw/main/Moyenne%20mobile/filtre2.png)

Par exemple, si on a les 4 dernières valent 5, 7, 7 et 9 et que la nouvelle mesure vaut 10

![signal lissé](https://gitlab.com/Peio47/premiere/-/raw/main/Moyenne%20mobile/filtre3.png)

alors la valeur filtrée vaudra 7.6 (moyenne des 5 valeurs) et la nouvelle fenêtre sera :

![signal lissé](https://gitlab.com/Peio47/premiere/-/raw/main/Moyenne%20mobile/filtre4.png)

**Compléter** la fonction `filtre()` qui prend en paramètres un tableau `valeurs` de 4 nombres entiers ou flottant ainsi qu'un nombre `nouvelle_mesure`, et qui renvoie le tableau modifié ainsi que la valeur filtrée.
